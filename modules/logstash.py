from cuisine import *
from fabric.contrib.files import upload_template
import os, jinja2

class LogStash():

    def __init__(self, configurations):
        self.core_host = configurations.get('General','host')

        self.ls_configuration_file_template = configurations.get('Templates','logstash_configuration')
        self.ls_init_file_template = configurations.get('Templates','logstash_initscript')

        self.core_packages_dir = configurations.get('Packages','directory')
        self.ls_package_url = configurations.get('Packages','logstash')

        self.ls_installation_dir = configurations.get('Installation','logstash_directory')
        self.ls_init_file = configurations.get('InitScripts','logstash')
        self.ls_configuration_file = configurations.get('Configurations','logstash')
        self.ls_configuration_dir = os.path.dirname(configurations.get('Configurations','logstash'))
        self.ls_log_dir = configurations.get('General','log_dir')
        self.ls_since_db_path = configurations.get('Specifications','logstash_since_db_path')
        self.es_index_name   = configurations.get('Specifications','elasticsearch_index_name')
 
        self.logstash_user="logstash"
        user_ensure(name=self.logstash_user)

    def install(self):
        with mode_sudo(True):
            self.__dependencies()
            self.__package_install()
            self.stop()
            self.__update_config()
            self.__update_init_script()

    def update(self):
        upstart_stop('logstash')
        print "update now..."
        upstart_ensure('logstash')

    def start(self):
        upstart_ensure('logstash')

    def stop(self):
        upstart_stop('logstash')

    def __dependencies(self):
        package_ensure('openjdk-7-jre')

    def __package_install(self):
        dir_ensure(self.ls_installation_dir, recursive=True)
        sudo("wget '%s' -O %s/logstash.jar" % (self.ls_package_url, self.core_packages_dir))
        sudo("cp %s/logstash.jar %s/" % (self.core_packages_dir, self.ls_installation_dir))
        logstash_binary = os.path.join(self.ls_installation_dir, "logstash.jar") 
        file_ensure(logstash_binary, mode="755", owner="root", group="root")

    def __update_config(self):
        context = {
            "elasticsearch_server": self.core_host,
            "elasticsearch_index_name": self.es_index_name,
        }
        self.stop()
        fptemplate = open(self.ls_configuration_file_template, 'r')
        template_content = fptemplate.read()
        config_content   = jinja2.Environment().from_string(template_content).render(context)
        file_write(self.ls_configuration_file, config_content)

    def __update_init_script(self):
        context = {
            "install_path": self.ls_installation_dir,
            "user": self.logstash_user,
            "conf_dir": self.ls_configuration_dir,
            "log_dir": self.ls_log_dir,
            "since_db_path": self.ls_since_db_path,
        }
        fptemplate = open(self.ls_init_file_template, 'r')
        template_content = fptemplate.read()
        config_content   = jinja2.Environment().from_string(template_content).render(context)
        file_write(self.ls_init_file, config_content)
        file_ensure(self.ls_init_file, mode="755", owner="root", group="root")
        dir_ensure(self.ls_log_dir, recursive=True)
        log_file = os.path.join(self.ls_log_dir, "logstash.log") 
        file_ensure(log_file, mode="755", owner=self.logstash_user, group=self.logstash_user)
        dir_ensure(self.ls_since_db_path, recursive=True)
        file_ensure(self.ls_since_db_path, owner=self.logstash_user, group=self.logstash_user)
        sudo("update-rc.d -f logstash defaults")
        sudo("update-rc.d -f logstash enable")
