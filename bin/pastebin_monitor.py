#! /usr/bin/env python

import logging
import regexconfig
import fileinput
import urllib
import re
import time
import sys
import signal
import json

class PastebinMonitor():

    keywords = dict()
    pastes_history = list()
    max_pastes_history = list()
    data_file = None
    log_file = None

    def get_pastes_list(self, paste_list_url, paste_list_exp):
	    try:
		    f = urllib.urlopen(paste_list_url)
		    pastes_list = re.findall(paste_list_exp, f.read())
		    f.close()
		    return pastes_list
	    except IOError:
		    logging.info("[!] Error fetching the list of pasties")
		    return []


    def clean_pastes_history(self):
	    while len(self.pastes_history) > self.max_pastes_history:
		    self.pastes_history.pop(0)
	
		    
    def get_paste(self, paste_id, paste_url):
	    param = urllib.urlencode({'i': paste_id})
	    logging.debug("\n[!] URL: http://pastebin.com/raw.php?%s" % param)
	    f = urllib.urlopen(paste_url % param)
	    paste_data = f.read()
            paste_data = unicode(paste_data, errors='ignore')
	    f.close()
	    return paste_data


    def process_paste(self, paste_data):
	    keywords_match = list()
	    for keyword in self.keywords.keys():
                for filter in self.keywords[keyword]:
                        times = len(re.findall(filter, paste_data,re.IGNORECASE))
                        if times > 0:
                                keywords_match.append(keyword)
                                break
	    return keywords_match		    


    def save_paste(self, paste_id, keywords_match, paste_data):
	    observation_time = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
            logging.info("\n[!] Found match at %s\n[!] Keywords: %s"  % (observation_time, ",".join(keywords_match)))
	    
	    data = dict()
	    data['url'] = "http://pastebin.com/" + paste_id
	    data['data'] = paste_data
	    data['keywords'] = list()
            for keyword in keywords_match:
                data['keywords'].append(keyword)

	    data_json = json.dumps(data)
	    
	    newline = "\n"
	    self.data_file.write(data_json)
	    self.data_file.write(newline)
            self.data_file.flush()

    def run(self, keywords_file, data_file, logging_file):

            rgp = regexconfig.RegExConfig()
            self.keywords = rgp.parse(keywords_file)
            self.data_file = open(data_file, 'a')
            logging.basicConfig(filename=logging_file, level=logging.DEBUG)

	    while True:	        
	        paste_url = "http://pastebin.com/raw.php?%s"
	        paste_list_url = "http://pastebin.com/archive"
	        paste_list_exp = '<td><img src="/i/t.gif"  class="i_p0" alt="" border="0" /><a href="/(\w+)">.+</a></td>'	        
	        wait_paste = 3
	        wait_pastes = 15
	        
	        for paste_id in self.get_pastes_list(paste_list_url, paste_list_exp):
		        if not paste_id in self.pastes_history:
			        try:
				        paste_data = self.get_paste(paste_id, paste_url)		    
				        keywords_match = self.process_paste(paste_data)
				        
				        if len(keywords_match) > 0:
					        self.save_paste(paste_id, keywords_match, paste_data)
				        self.pastes_history.append(paste_id)
				        
				        time.sleep(wait_paste)
			        except IOError:
				        logging.info("[!] Error fetching paste %s" % paste_id)

	        self.clean_pastes_history()

	        time.sleep(wait_pastes)
	        logging.info("\n\n[+] Downloading pastebin/archive at " + time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()))

            self.data_file.close()
	        
		    
if __name__ == "__main__":
    keywords_file = sys.argv[1]
    data_file = '/opt/pastebin_monitor/pastebin_monitor.data'
    logging_file = '/opt/pastebin_monitor/pastebin_monitor.log'

    pmonitor = PastebinMonitor()	    
    pmonitor.run(keywords_file, data_file, logging_file)

